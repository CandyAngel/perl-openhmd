#!/usr/bin/env perl

use strict;
use warnings;

use Test::More (
    tests   => 1,
);

BEGIN {
    use_ok ('OpenHMD::Constants') or BAIL_OUT ('Failed to load module');
};
