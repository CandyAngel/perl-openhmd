#!/usr/bin/env perl

use strict;
use warnings;

use Test::More (
    tests   => 21,
);

BEGIN {
    use_ok ('OpenHMD::Inline') or BAIL_OUT ('Failed to load module');
};

my @METHODS = qw(
    ohmd_ctx_create
    ohmd_ctx_destroy
    ohmd_ctx_get_error
    ohmd_ctx_probe
    ohmd_ctx_update
    ohmd_device_getf
    ohmd_device_geti
    ohmd_device_setf
    ohmd_list_gets
    ohmd_list_open_device

    inline_ctx_create
    inline_ctx_destroy
    inline_ctx_get_error
    inline_ctx_probe
    inline_ctx_update
    inline_device_getf
    inline_device_geti
    inline_device_setf
    inline_list_gets
    inline_list_open_device
);
foreach my $method (@METHODS) {
    can_ok ('OpenHMD::Inline', $method);
}
