#!/usr/bin/env perl

use strict;
use warnings;

use OpenHMD::Constants  qw(:all);
use OpenHMD::Inline     qw(:all);

main();

sub ohmd_sleep {
    select (undef, undef, undef, shift);
};

sub print_infof {
    my ($hmd, $name, $len, $val) = @_;
    my @f = ohmd_device_getf($hmd, $val);
    printf("%-20s", $name);
    for(my $i = 0; $i < $len; $i++) {
        printf("%f ", $f[$i]);
    }
    printf("\n");
}

sub main {
    my $ctx = ohmd_ctx_create();

    # Probe for devices
    my $num_devices = ohmd_ctx_probe($ctx);
    if ($num_devices < 0) {
        printf("failed to probe devices: %s\n", ohmd_ctx_get_error($ctx));
        return;
    }
    printf("num devices: %d\n\n", $num_devices);

    # Print device information
    for (my $i = 0; $i < $num_devices; $i++) {
        printf("device %d\n", $i);
        printf(" vendor: %s\n", ohmd_list_gets($ctx, $i, $OHMD_VENDOR));
        printf(" product: %s\n", ohmd_list_gets($ctx, $i, $OHMD_PRODUCT));
        printf(" path: %s\n\n", ohmd_list_gets($ctx, $i, $OHMD_PATH));
    }

    # Open default device (0)
    my $hmd = ohmd_list_open_device($ctx, 0);
    if (!$hmd) {
        printf("failed to open device: %s\n", ohmd_ctx_get_error($ctx));
        return;
    }

    # Print hardware information for the opened device
    my @ivals;
    $ivals[0] = ohmd_device_geti($hmd, $OHMD_SCREEN_HORIZONTAL_RESOLUTION);
    $ivals[1] = ohmd_device_geti($hmd, $OHMD_SCREEN_VERTICAL_RESOLUTION);
    printf("resolution: %i x %i\n", $ivals[0], $ivals[1]);
    print_infof($hmd, "hsize:", 1, $OHMD_SCREEN_HORIZONTAL_SIZE);
    print_infof($hmd, "vsize:", 1, $OHMD_SCREEN_VERTICAL_SIZE);
    print_infof($hmd, "lens separation:", 1, $OHMD_LENS_HORIZONTAL_SEPARATION);
    print_infof($hmd, "lens vcenter:", 1, $OHMD_LENS_VERTICAL_POSITION);
    print_infof($hmd, "left eye fov:", 1, $OHMD_LEFT_EYE_FOV);
    print_infof($hmd, "right eye fov:", 1, $OHMD_RIGHT_EYE_FOV);
    print_infof($hmd, "left eye aspect:", 1, $OHMD_LEFT_EYE_ASPECT_RATIO);
    print_infof($hmd, "right eye aspect:", 1, $OHMD_RIGHT_EYE_ASPECT_RATIO);
    print_infof($hmd, "distortion k:", 6, $OHMD_DISTORTION_K);
    printf("\n");

    # Ask for n rotation quaternions
    for (my $i = 0; $i < 10000; $i++) {
        ohmd_ctx_update($ctx);
        print_infof($hmd, "rotation quat:", 4, $OHMD_ROTATION_QUAT);
        ohmd_sleep(.01);
    }

    ohmd_ctx_destroy($ctx);
}
