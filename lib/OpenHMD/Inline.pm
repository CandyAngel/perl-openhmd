package OpenHMD::Inline;

use strict;
use warnings;

our $VERSION = 0.001_000;

use Exporter qw(import);
use OpenHMD::Constants qw(:all);

use Inline C => Config => (
    LIBS => '-lopenhmd',
);
use Inline 'C';

our %EXPORT_TAGS = (
    'all' => [qw(
        ohmd_ctx_create
        ohmd_ctx_destroy
        ohmd_ctx_get_error
        ohmd_ctx_probe
        ohmd_ctx_update
        ohmd_device_getf
        ohmd_device_geti
        ohmd_device_setf
        ohmd_list_gets
        ohmd_list_open_device
    )],
);

Exporter::export_ok_tags ('all');


sub ohmd_ctx_create {
    return inline_ctx_create();
}

sub ohmd_ctx_destroy {
    my $context = shift;

    return inline_ctx_destroy($context);
}

sub ohmd_ctx_get_error {
    my $context = shift;

    return inline_ctx_get_error($context);
}

sub ohmd_ctx_probe {
    my $context = shift;

    return inline_ctx_probe($context);
}

sub ohmd_ctx_update {
    my $context = shift;

    return inline_ctx_update($context);
}

sub ohmd_device_getf {
    my ($device, $type) = @_;

    my @values = inline_device_getf($device, $type);

    my %size = (
        $OHMD_DISTORTION_K                      =>  6,
        $OHMD_EYE_IPD                           =>  1,
        $OHMD_LEFT_EYE_ASPECT_RATIO             =>  1,
        $OHMD_LEFT_EYE_FOV                      =>  1,
        $OHMD_LEFT_EYE_GL_MODELVIEW_MATRIX      => 16,
        $OHMD_LEFT_EYE_GL_PROJECTION_MATRIX     => 16,
        $OHMD_LENS_HORIZONTAL_SEPARATION        =>  1,
        $OHMD_LENS_VERTICAL_POSITION            =>  1,
        $OHMD_POSITION_VECTOR                   =>  3,
        $OHMD_PROJECTION_ZFAR                   =>  1,
        $OHMD_PROJECTION_ZNEAR                  =>  1,
        $OHMD_RIGHT_EYE_ASPECT_RATIO            =>  1,
        $OHMD_RIGHT_EYE_FOV                     =>  1,
        $OHMD_RIGHT_EYE_GL_MODELVIEW_MATRIX     => 16,
        $OHMD_RIGHT_EYE_GL_PROJECTION_MATRIX    => 16,
        $OHMD_ROTATION_QUAT                     =>  4,
        $OHMD_SCREEN_HORIZONTAL_SIZE            =>  1,
        $OHMD_SCREEN_VERTICAL_SIZE              =>  1,
    );

    if (exists $size{$type}) {
        my $index = $size{$type} - 1;
        return @values[0 .. $index];
    }

    return @values;
}

sub ohmd_device_geti {
    my ($device, $type) = @_;

    return inline_device_geti($device, $type);
}

sub ohmd_device_setf {
    my ($device, $type, $value) = @_;

    return inline_device_setf($device, $type, $value);
}

sub ohmd_list_gets {
    my ($context, $index, $type) = @_;

    return inline_list_gets($context, $index, $type);
}

sub ohmd_list_open_device {
    my ($context, $index) = @_;

    return inline_list_open_device($context, $index);
}

1;

__DATA__
__C__

#include "openhmd/openhmd.h"

int inline_ctx_create() {
    return ohmd_ctx_create();
}

void inline_ctx_destroy(int ctx) {
    ohmd_ctx_destroy(ctx);
}

char * inline_ctx_get_error(int ctx) {
    return ohmd_ctx_get_error(ctx);
}

int inline_ctx_probe(int ctx) {
    return ohmd_ctx_probe(ctx);
}

void inline_ctx_update(int ctx) {
    ohmd_ctx_update(ctx);
}

void inline_device_getf(int device, int type) {
    float buffer[16];

    ohmd_device_getf(device, type, buffer);

    Inline_Stack_Vars;
    Inline_Stack_Reset;

    int i;
    for(i = 0; i < 16; i++) {
        Inline_Stack_Push(sv_2mortal(newSVnv(buffer[i])));
    }

    Inline_Stack_Done;
}

int inline_device_geti(int device, int type) {
    int buffer[1];

    ohmd_device_geti(device, type, buffer);

    return buffer[0];
}

int inline_device_setf(int device, int type, float in) {
    return ohmd_device_setf(device, type, &in);
}

char * inline_list_gets(int ctx, int index, int type) {
    return ohmd_list_gets(ctx, index, type);
}

int inline_list_open_device(int ctx, int index) {
    return ohmd_list_open_device(ctx, index);
}
