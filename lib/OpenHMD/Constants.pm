package OpenHMD::Constants;

use strict;
use warnings;

our $VERSION = 0.001_000;

use Const::Fast;
use Exporter 'import';

const our $OHMD_VENDOR                          =>  0;
const our $OHMD_PRODUCT                         =>  1;
const our $OHMD_PATH                            =>  2;

const our $OHMD_ROTATION_QUAT                   =>  1;
const our $OHMD_LEFT_EYE_GL_MODELVIEW_MATRIX    =>  2;
const our $OHMD_RIGHT_EYE_GL_MODELVIEW_MATRIX   =>  3;
const our $OHMD_LEFT_EYE_GL_PROJECTION_MATRIX   =>  4;
const our $OHMD_RIGHT_EYE_GL_PROJECTION_MATRIX  =>  5;
const our $OHMD_POSITION_VECTOR                 =>  6;
const our $OHMD_SCREEN_HORIZONTAL_SIZE          =>  7;
const our $OHMD_SCREEN_VERTICAL_SIZE            =>  8;
const our $OHMD_LENS_HORIZONTAL_SEPARATION      =>  9;
const our $OHMD_LENS_VERTICAL_POSITION          => 10;
const our $OHMD_LEFT_EYE_FOV                    => 11;
const our $OHMD_LEFT_EYE_ASPECT_RATIO           => 12;
const our $OHMD_RIGHT_EYE_FOV                   => 13;
const our $OHMD_RIGHT_EYE_ASPECT_RATIO          => 14;
const our $OHMD_EYE_IPD                         => 15;
const our $OHMD_PROJECTION_ZFAR                 => 16;
const our $OHMD_PROJECTION_ZNEAR                => 17;
const our $OHMD_DISTORTION_K                    => 18;

const our $OHMD_SCREEN_HORIZONTAL_RESOLUTION    =>  0;
const our $OHMD_SCREEN_VERTICAL_RESOLUTION      =>  1;

our %EXPORT_TAGS = (
    'all' => [qw(
        $OHMD_VENDOR
        $OHMD_PRODUCT
        $OHMD_PATH

        $OHMD_ROTATION_QUAT
        $OHMD_LEFT_EYE_GL_MODELVIEW_MATRIX
        $OHMD_RIGHT_EYE_GL_MODELVIEW_MATRIX
        $OHMD_LEFT_EYE_GL_PROJECTION_MATRIX
        $OHMD_RIGHT_EYE_GL_PROJECTION_MATRIX
        $OHMD_POSITION_VECTOR
        $OHMD_SCREEN_HORIZONTAL_SIZE
        $OHMD_SCREEN_VERTICAL_SIZE
        $OHMD_LENS_HORIZONTAL_SEPARATION
        $OHMD_LENS_VERTICAL_POSITION
        $OHMD_LEFT_EYE_FOV
        $OHMD_LEFT_EYE_ASPECT_RATIO
        $OHMD_RIGHT_EYE_FOV
        $OHMD_RIGHT_EYE_ASPECT_RATIO
        $OHMD_EYE_IPD
        $OHMD_PROJECTION_ZFAR
        $OHMD_PROJECTION_ZNEAR
        $OHMD_DISTORTION_K

        $OHMD_SCREEN_HORIZONTAL_RESOLUTION
        $OHMD_SCREEN_VERTICAL_RESOLUTION
    )],
);

Exporter::export_ok_tags ('all');

1;
